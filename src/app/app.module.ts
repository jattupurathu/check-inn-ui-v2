import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ReserveGridComponent } from './reserve-grid/reserve-grid.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common'

// for prime ng modules
import { BreadcrumbModule,ButtonModule,MenubarModule,TieredMenuModule,
          MenuModule,DataTableModule, 
          InputTextareaModule, PanelModule, DropdownModule, MessageModule,
          MessagesModule,ContextMenuModule,GrowlModule,FieldsetModule,
          CalendarModule,SharedModule,SidebarModule,CheckboxModule,TabViewModule,InputTextModule,
          OverlayPanelModule,TabMenuModule,SelectButtonModule} from 'primeng/primeng';
import { NewBookingComponent } from './new-booking/new-booking.component';
import { RoomHistoryComponent } from './room-history/room-history.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';




@NgModule({
  declarations: [
    AppComponent,
    ReserveGridComponent,
    HeaderComponent,
    NewBookingComponent,
    RoomHistoryComponent,
    LandingComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule, 
    ReactiveFormsModule,
    TieredMenuModule,
    MessageModule,
    MessagesModule,
    GrowlModule,
    ContextMenuModule,
    MenuModule,
    MenubarModule,
    DataTableModule,
    SharedModule,
    SidebarModule,
    InputTextareaModule, 
    PanelModule, 
    DropdownModule,
    ButtonModule,
    FieldsetModule,
    BrowserAnimationsModule,
    CalendarModule,
    CheckboxModule,
    TabViewModule,
    OverlayPanelModule,
    TabMenuModule,
    BreadcrumbModule,
    SelectButtonModule,
    RouterModule.forRoot([
      {
        path:'',
        component:LoginComponent
      },
      {
        path:'landing',
        component:LandingComponent
      },
      {
        path:'reservation',
        component:ReserveGridComponent
      },
      {
        path:'new-reservation',
        component:RoomHistoryComponent
      }
    ])

  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
