import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Message,MenuItem} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';


@Component({
  selector: 'app-reserve-grid',
  templateUrl: './reserve-grid.component.html',
  styleUrls: ['./reserve-grid.component.css']
})
export class ReserveGridComponent implements OnInit {

  private items: MenuItem[];

  reservations:Reservations[];
  reserveItems: MenuItem[];
  msgs: Message[];
  selectedReservations: Reservations;
  filter: SelectItem[];
  selectedFilter: string[] = [];

 constructor(private http: HttpClient){
    this.filter = [];
    this.filter.push({label:'Only Blank Guest Details', value:'BGD'});
    this.filter.push({label:'Only Cancelled Booking', value:'OCB'});
    this.filter.push({label:'Pending Allocations', value:'PA'});
    this.filter.push({label:'All Users Booking', value:'AUB'});
  }

  ngOnInit() {
    this.items = [
      {label:'Reservation'}
    ];
    this.http.get<Reservations[]>('http://localhost:8080/check-inn/reservation/').subscribe(data => {
      this.reservations=data;
    });
    this.reserveItems = [
      {label: 'View Booking', icon: 'fa-search', command: (event) => this.viewReservations(this.selectedReservations)},
      {label: 'Edit Booking', icon: 'fa-edit', command: (event) => this.viewReservations(this.selectedReservations)},
      {label: 'Edit Guest Details', icon: 'fa-edit', command: (event) => this.viewReservations(this.selectedReservations)},
      {label: 'Track Guest', icon: 'fa-check', command: (event) => this.viewReservations(this.selectedReservations)},
      {label: 'Guest History', icon: 'fa-mail-reply', command: (event) => this.viewReservations(this.selectedReservations)}
    ];

  }
  viewReservations(reservations:Reservations) {
    console.log(reservations);
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Reservation Selected', detail: reservations.bookingid + ' - ' + reservations.guestname});
  }

}

interface Reservations{
  bookingid:string;
  canceledreason:string;
  guestname:string;
  checkindate:string;
  status:string;
  canceledid: string;
  attendedby:string;
  checkoutdate:string;
  allocation:string;
  bookingdate:string;
}

