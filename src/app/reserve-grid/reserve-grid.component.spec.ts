import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserveGridComponent } from './reserve-grid.component';

describe('ReserveGridComponent', () => {
  let component: ReserveGridComponent;
  let fixture: ComponentFixture<ReserveGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserveGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserveGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
