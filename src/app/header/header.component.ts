import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/primeng';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[];
  
  constructor() { }

  ngOnInit() {
    this.items = [
      {label: 'Reservations', icon: 'fa-group'},
      {label: 'Check-inn', icon: 'fa-check-circle-o'},
      {label: 'Masters', icon: ' fa-cogs'},
      {label: 'Receipts', icon: 'fa-file'},
      {label: 'Reports', icon: 'fa-paperclip'}
  ];
  }

}
