import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message,Messages } from 'primeng/primeng';
import {MenuItem} from 'primeng/primeng';
import { DatePipe } from '@angular/common';
import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-room-history',
  templateUrl: './room-history.component.html',
  styleUrls: ['./room-history.component.css']
})
export class RoomHistoryComponent implements OnInit {
  private items: MenuItem[];
  checkinValue:Date;
  checkoutValue:Date;
  daysMsg:Message[]=[];
  availableRooms:AvailableRooms[];
  days:number=0;
  timeDiff:number=0;
  totalRoomCount:TotalRoomCount[];
  selectedValues: string[] = [];
  bookingID:string="";
  selectedStatus:string="";
  status:SelectItem[];
  agents:SelectItem[];
  selectedAgent:string="";
  selectedCompany:string="";
  company:SelectItem[];
  billTo:SelectItem[];
  agentCompany:AgentList[];
  selectedTAC:number[]=[];

  constructor(private http: HttpClient, private datepipe: DatePipe) { 
  }

  ngOnInit() {
    this.items = [
      {label:'Reservation', routerLink:['/reservation']},
      {label:'New Reservation'}
    ];

    this.selectedTAC=[0];
    this.status = [
      {label:'Confirmed', value:1},
      {label:'Provisional', value:2},
      {label:'Waiting', value:3}
    ]

    this.agents = [
      {label:'Self', value:1}
    ]

    this.company = [
      {label:'Self', value:1}
    ]

    this.billTo = [
      {label:'Self', value:1}
    ]

    this.http.get<TotalRoomCount[]>('http://localhost:8080/check-inn/reservation/totalcount/').subscribe(data => {
      this.totalRoomCount=data;
    });

    this.http.get<AgentList[]>('http://localhost:8080/check-inn/agents/').subscribe(data => {
      //this.agentCompany=data;
      for(var i=0;i<data.length;i++){
        this.agents.push({label:data[i].agentFirstName+' '+data[i].agentLastName, value:1});
        
      }
    });
    this.http.get<AgentList[]>('http://localhost:8080/check-inn/agents/').subscribe(data => {
      this.agentCompany=data;
      for(var i=0;i<data.length;i++){
        this.company.push({label:data[i].agentFirstName+' '+data[i].agentLastName, value:1});
      }
    });
  }   

  dateSelect(){
    if(this.checkinValue!=undefined && this.checkoutValue!=undefined){
      this.timeDiff=this.checkoutValue.getTime() - this.checkinValue.getTime();
      this.days = Math.ceil(this.timeDiff / (1000 * 3600 * 24)); 
      if(this.days<0){
        this.daysMsg.push({severity:'error', summary:'Error Message', detail:'Checkin Date cannot be after Checkout Date'});
      }else if(this.days==0){
        this.daysMsg.push({severity:'error', summary:'Error Message', detail:'Atleast 1 Day stay selection should be done'});
      }else{
        this.daysMsg=[];
        this.http.get<AvailableRooms[]>('http://localhost:8080/check-inn/reservation/availableRooms/'+this.datepipe.transform(this.checkinValue, 'yyyy-MM-dd')+'/'+this.datepipe.transform(this.checkoutValue, 'yyyy-MM-dd')).subscribe(data => {
          this.availableRooms=data;
        });
        
      }
    }
  }

}

interface TotalRoomCount{
  total:number;
  confirmed:number;
  waiting:number;
  provisional:number; 
}

interface AvailableRooms{
  day:string;
  date:string;
  dlx:number;
  sdx:number;
  cot:number;
  std:number;
  ffb:number;
  sui:number;
  total:number;
}

interface AgentList{
  agentID:string;
  agentFirstName:string;
  agentLastName:string;
  agentPhone:string;
  agentEmail:string;
  agentType:number;
  agentTAC:number;
}

